# EVIDENCIA STATE MANAGMENT CON RIDUX Y REACT

Este proyecto fue iniciado con Crate React App y consiste en un ejemplo para
evidenciar el trabajo de **Redux**.

El objetivo final es evidenciar el trabajo realizado con Redux. Consiste en un
ejemplo simple de un contador utilizando la librería **_react-redux_** y
**@reduxjs/toolkit**.

En este ejemplo encontraremos las siguientes secciones divididas por carpetas:

`actions:` En esta carpeta se encuentran todos los módulos js con los actions
que utilizarán los reducers.

`reducers:` En esta carpete se encuentran todos los reducers que tengamos.

`store:` En esta carpeta se encuentran el store correspondiente.

`types:` En esta carpeta encontraremos constantes con todos los posibles types
que viajaran en el actions.

## Instrucciones

### Pasos para configurar nuestro proyecto:

1. Instalar dependencias del proyecto. Nos dirigimos a la ruta donde descargamos
   el repositorio y tipeamos el comando **_npm install_** Aparte de las
   dependencias propia de react también encontraremos las dependencias de
   reac-redux y @reduxjs/toolkit

> Bonus track: Se encuentra adjunto un archivo de configuración de prettier.
