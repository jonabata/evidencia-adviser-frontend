import { DECREMENT, INCREMENT, INCREMENT_5, RESET } from "../types"

export const increment = ()=>({type: INCREMENT});
export const decrement = ()=>({type: DECREMENT});
export const increment_5 = (payload)=>({type: INCREMENT_5, payload: payload});
export const reset = ()=>({type: RESET});