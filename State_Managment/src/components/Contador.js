import { useSelector, useDispatch } from "react-redux/es/exports"
import { decrement, increment, increment_5, reset } from "../actions/contadorAction";
const Contador = ()=>{
    const state = useSelector(state=>state)    
    const dispatch =  useDispatch();
    return(
        <div className="contadorContainer">
            <h3>CONTADOR</h3>
            <h2>{state.contador}</h2>
            <button onClick={()=>dispatch(decrement())}>-</button>
            <button onClick={()=>dispatch(reset())}>0</button>
            <button onClick={()=>dispatch(increment())}>+</button>
            <button onClick={()=>dispatch(increment_5(5))}>+5</button>
        </div>
    )
}

export default Contador