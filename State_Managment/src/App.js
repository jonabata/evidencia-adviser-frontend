import Contador from "./components/Contador";
import { Provider } from "react-redux";
import store from "./store"
function App() {
  return (
      <div className="App">
      <Provider store={store}>
            <Contador/>
      </Provider>
      </div>
  );
}

export default App;
