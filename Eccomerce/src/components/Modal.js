import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import ShoppingContext from '../context/ShoppingContext';

export default function Modal({ children, title, submitIsVisible = true }) {
	const { open, scroll, handleClose } = React.useContext(ShoppingContext);

	const descriptionElementRef = React.useRef(null);
	React.useEffect(() => {
		if (open) {
			const { current: descriptionElement } = descriptionElementRef;
			if (descriptionElement !== null) {
				descriptionElement.focus();
			}
		}
	}, [open]);

	return (
		<>
			<div>
				<Dialog
					open={open}
					onClose={handleClose}
					scroll={scroll}
					aria-labelledby='scroll-dialog-title'
					aria-describedby='scroll-dialog-description'>
					<DialogTitle id='scroll-dialog-title'>{title}</DialogTitle>
					<DialogContent dividers={scroll === 'paper'}>
						<DialogContentText
							id='scroll-dialog-description'
							ref={descriptionElementRef}
							tabIndex={-1}>
							{children}
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={handleClose}>Cancelar</Button>
						{submitIsVisible == true && (
							<Button onClick={handleClose}>Confirmar</Button>
						)}
					</DialogActions>
				</Dialog>
			</div>
		</>
	);
}
