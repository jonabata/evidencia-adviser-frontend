const Footer = () => {
	return (
		<footer className='footer'>
			<p>© Copyright Jonathan González</p>
		</footer>
	);
};

export default Footer;
