import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
const Messages = ({ mensaje, tipo, title }) => {
	return (
		<Stack sx={{ width: '100%' }} spacing={2}>
			<Alert severity={tipo}>
				<AlertTitle>{title}</AlertTitle>
				{mensaje}
			</Alert>
		</Stack>
	);
};
export default Messages;
