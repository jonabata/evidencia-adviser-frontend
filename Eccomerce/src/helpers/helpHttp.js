export const helpHttp = () => {
	const customFetch = (endpoint, options) => {
		const defaultHeader = {
			acept: 'application/json',
		};

		const controller = new AbortController();

		options.signal = controller.signal;

		options.method = options.method || 'GET';

		options.header = options.header
			? { ...defaultHeader, ...options.header }
			: defaultHeader;

		options.body = JSON.stringify(options.body) || false;

		if (!options.body) {
			delete options.body;
		}

		setTimeout(() => controller.abort(), 30000);

		return fetch(endpoint, options)
			.then((res) =>
				res.ok
					? res.json()
					: Promise.reject({
							err: true,
							status: res.status || '00',
							statusText: res.statusText || 'Ocurrió un error',
					  })
			)
			.catch((err) => err);
	};

	const get = (url, options = {}) => customFetch(url, options);

	return {
		get,
	};
};
