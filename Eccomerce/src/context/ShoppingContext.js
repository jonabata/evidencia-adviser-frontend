import React, { createContext, useState, useEffect } from 'react';
import { useSnackbar } from 'notistack';
import { helpHttp } from '../helpers/helpHttp';

const ShoppingContext = createContext();
const initialShoppingState = {
	products: [],
	cart: [],
	quantityInCart: 0,
};

const ShoppingProvider = ({ children }) => {
	const [products, setDb] = useState(initialShoppingState.products);
	const [cart, setCart] = useState(initialShoppingState.cart);
	const [search, setSearch] = useState([]);
	const [open, setOpen] = useState(false);
	const [openBackDrop, setOpenBackDrop] = useState(false);
	const [scroll, setScroll] = useState('paper');
	const [quantityInCart, setquantityInCart] = useState(
		initialShoppingState.quantityInCart
	);
	const { enqueueSnackbar, closeSnackbar } = useSnackbar();
	const [isLoged, setisLoged] = useState(false);
	const url = 'http://localhost:5000/products';

	const handleToggle = () => {
		setOpenBackDrop(!openBackDrop);
	};

	useEffect(() => {
		if (quantityInCart == 0) {
			setOpen(false);
		}
		setOpenBackDrop(true);

		setTimeout(() => {
			helpHttp()
				.get(url)
				.then((res) => {
					if (!res.err) {
						setDb(res);
						setOpenBackDrop(false);
					} else {
					}
				});
		}, 3000);
	}, [url]);

	const addToCart = (id) => {
		let newItems = products.find((products) => products.id === id);

		let itemInCart = cart.find((item) => item.id === id);

		if (itemInCart) {
			setCart(
				cart.map((item) =>
					item.id === newItems.id
						? { ...item, quantity: item.quantity + 1 }
						: item
				)
			);
		} else {
			setCart([...cart, { ...newItems, quantity: 1 }]);
		}

		setDb(
			products.map((item) =>
				item.id === id ? { ...item, stock: item.stock - 1 } : item
			)
		);
		setquantityInCart(quantityInCart + 1);

		enqueueSnackbar('Agregado', { variant: 'success' });
	};

	const getItemsCartStorage = () => {
		if (localStorage.getItem('cart')) {
			return JSON.parse(localStorage.getItem('cart'));
		} else {
			return [];
		}
	};

	const handleClickOpen = (scrollType) => () => {
		setOpen(true);
		setScroll(scrollType);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const deleteAllToCart = (id, cantidad) => {
		let newItemsCart = cart.filter((item) => item.id != id);
		setCart(newItemsCart);
		setquantityInCart(quantityInCart - cantidad);
		setDb(
			products.map((item) =>
				item.id === id ? { ...item, stock: item.stock + cantidad } : item
			)
		);
	};

	const itemIncrement = (id) => {
		let stockDB = products.filter((item) => item.id == id && item.stock > 0);
		if (stockDB.length > 0) {
			setCart(
				cart.map((item) =>
					item.id === id ? { ...item, quantity: item.quantity + 1 } : item
				)
			);
			setDb(
				products.map((item) =>
					item.id === id ? { ...item, stock: item.stock - 1 } : item
				)
			);
			setquantityInCart(quantityInCart + 1);
		} else {
			enqueueSnackbar('No hay stock', { variant: 'error' });
		}
	};

	const itemDecrement = (id) => {
		setCart(
			cart.map((item) =>
				item.id === id ? { ...item, quantity: item.quantity - 1 } : item
			)
		);
		setDb(
			products.map((item) =>
				item.id === id ? { ...item, stock: item.stock + 1 } : item
			)
		);
		setquantityInCart(quantityInCart - 1);
	};

	const handleChangeSearch = (e) => {
		let texto = e.target.value.toLowerCase();
		if (texto.length > 1) {
			for (let producto of products) {
				let nombre = producto.name.toLocaleLowerCase();
				if (nombre.indexOf(texto) !== -1) {
					console.log(producto.name);
				}
			}
		}
	};

	const data = {
		products,
		cart,
		open,
		openBackDrop,
		quantityInCart,
		isLoged,
		search,
		addToCart,
		itemIncrement,
		itemDecrement,
		deleteAllToCart,
		handleClickOpen,
		handleClose,
		handleChangeSearch,
	};
	return (
		<ShoppingContext.Provider value={data}>{children}</ShoppingContext.Provider>
	);
};

export { ShoppingProvider };
export default ShoppingContext;
