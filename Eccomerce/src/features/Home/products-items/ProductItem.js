import IconButton from '@mui/material/IconButton';
import VisibilityIcon from '@mui/icons-material/Visibility';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import { Backdrop, CircularProgress, Typography } from '@mui/material';
import { useContext } from 'react';
import ShoppingContext from '../../../context/ShoppingContext';
import Messages from '../../../components/Messages';

const ProductItem = () => {
	const { products, addToCart, openBackDrop } = useContext(ShoppingContext);
	const stock = products.filter((item) => item.stock > 0);
	return (
		<article className='container-items'>
			<h4>Productos:</h4>
			<article className='grid-nested'>
				{products.map(
					(producto) =>
						producto.stock > 0 && (
							<div key={producto.id}>
								<div className='img-item-conteiner'>
									<img
										src={'../images/products-images/' + producto.img}
										className='img-items'
										alt={producto.name}
									/>
								</div>
								<Typography variant='h4' gutterBottom component='div'>
									{new Intl.NumberFormat('es-AR', {
										currency: 'ARS',
										style: 'currency',
									}).format(parseFloat(producto.precio))}
								</Typography>
								<Typography variant='h6' gutterBottom component='div'>
									{producto.name}
								</Typography>
								<p>{producto.descripcion}</p>
								<footer>
									<IconButton
										color='primary'
										aria-label='Agregar al carrito'
										onClick={() => addToCart(producto.id)}>
										<AddShoppingCartIcon />
									</IconButton>
									<IconButton color='success' aria-label='Ver producto'>
										<VisibilityIcon />
									</IconButton>
								</footer>
							</div>
						)
				)}
			</article>
			{stock.length == 0 && (
				<Messages
					title='Ops!'
					mensaje='No hay productos para mostrar'
					tipo='warning'
				/>
			)}
			<div>
				<Backdrop
					sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
					open={openBackDrop}>
					<CircularProgress color='inherit' />
				</Backdrop>
			</div>
		</article>
	);
};
export default ProductItem;
