import { useParams } from 'react-router-dom';
const ProductDetails = () => {
	let { id, name } = useParams();
	return <h1>{name}</h1>;
};

export default ProductDetails;
