import * as React from 'react';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import TextField from '@mui/material/TextField';
import { Button, FormHelperText } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';

const FormContac = () => {
	const { enqueueSnackbar, closeSnackbar } = useSnackbar();
	return (
		<Formik
			initialValues={{
				contactName: '',
				email: '',
				comments: '',
			}}
			validate={(inputsValues) => {
				let errors = {};
				if (!inputsValues.contactName) {
					errors.contactName = 'Por favor ingrese un nombre';
				} else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(inputsValues.contactName)) {
					errors.contactName =
						'El nombre solo puede contener letras y espacios';
				}

				if (!inputsValues.email) {
					errors.email = 'Por favor ingrese un email';
				} else if (
					!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(
						inputsValues.email
					)
				) {
					errors.email = 'Por favor ingrese un mail válido';
				}
				if (!inputsValues.comments) {
					errors.comments = 'Por favor ingrese un comentario';
				}
				return errors;
			}}
			onSubmit={(values, { resetForm }) => {
				enqueueSnackbar('Mensaje enviado', { variant: 'success' });
				resetForm();
			}}>
			{({
				values,
				errors,
				touched,
				handleChange,
				handleSubmit,
				handleBlur,
			}) => (
				<form className='container-items' onSubmit={handleSubmit}>
					<section className='container-form-contact'>
						<TextField
							id='contactName'
							label='Nombre'
							variant='standard'
							sx={{ maxWidth: 400 }}
							value={values.contactName}
							onChange={handleChange}
							onBlur={handleBlur}
						/>
						{touched.contactName && errors.contactName && (
							<FormHelperText
								id='component-error-text'
								error
								style={{ marginTop: '-19px' }}>
								{errors.contactName}
							</FormHelperText>
						)}
						<TextField
							id='email'
							label='Email'
							variant='standard'
							sx={{ maxWidth: 400 }}
							value={values.email}
							onChange={handleChange}
							onBlur={handleBlur}
						/>
						{touched.email && errors.email && (
							<FormHelperText
								id='component-error-text'
								error
								style={{ marginTop: '-19px' }}>
								{errors.email}
							</FormHelperText>
						)}
						<TextField
							id='comments'
							label='Comentarios'
							multiline
							rows={5}
							fullWidth
							value={values.comments}
							onChange={handleChange}
							onBlur={handleBlur}
						/>
						{touched.comments && errors.comments && (
							<FormHelperText
								id='component-error-text'
								error
								style={{ marginTop: '-19px' }}>
								{errors.comments}
							</FormHelperText>
						)}
						<footer>
							<Button
								variant='contained'
								endIcon={<SendIcon />}
								sx={{ width: 110 }}
								type='submit'>
								Enviar
							</Button>
							{/* <Button 
                        variant="contained" 
                        endIcon={<CleaningServicesIcon />} 
                        sx={{width: 110, marginLeft: 1}}
                        color="warning"
                    >
                        Limpiar
                    </Button> */}
						</footer>
					</section>
				</form>
			)}
		</Formik>
	);
};

export default FormContac;
