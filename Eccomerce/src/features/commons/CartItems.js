import { Button, ButtonGroup, IconButton } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import { useContext } from 'react';
import ShoppingContext from '../../context/ShoppingContext';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import Modal from '../../components/Modal';
import Messages from '../../components/Messages';

const CartItems = () => {
	const {
		cart,
		deleteAllToCart,
		itemIncrement,
		itemDecrement,
		quantityInCart,
	} = useContext(ShoppingContext);
	return (
		<Modal
			title='Carrito de compras'
			submitIsVisible={quantityInCart > 0 || 'false'}>
			<article className='items-cart-container'>
				{quantityInCart > 0 &&
					cart.map((item) => (
						<div className='item-cart' key={item.id}>
							<div className='img-cart-item-conteiner'>
								<img
									src={'../images/products-images/' + item.img}
									className='img-items'
								/>
							</div>
							<p>{item.name}</p>
							<span className='items-container-buttons'>
								<IconButton
									aria-label='decrement-one'
									color='primary'
									disabled={item.quantity == 1 ? true : false}
									onClick={() => itemDecrement(item.id)}>
									<RemoveCircleOutlineIcon />
								</IconButton>
								<span>{item.quantity}</span>
								<IconButton
									aria-label='increment-one'
									color='primary'
									onClick={() => itemIncrement(item.id)}>
									<AddCircleOutlineIcon />
								</IconButton>
								<IconButton
									aria-label='delete'
									color='warning'
									onClick={() => deleteAllToCart(item.id, item.quantity)}>
									<DeleteIcon />
								</IconButton>
							</span>
						</div>
					))}
				{quantityInCart == 0 && (
					<Messages
						title='😱'
						mensaje='El carro de compras está vacío'
						tipo='warning'
					/>
				)}
			</article>
		</Modal>
	);
};
export default CartItems;
