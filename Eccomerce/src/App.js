import { ShoppingProvider } from './context/ShoppingContext';
import { SnackbarProvider } from 'notistack';
import { HashRouter, Route, Routes } from 'react-router-dom';
import Nav from './components/Nav';
import Contacto from './features/contact/';
import Home from './features/Home';
import ProductDetails from './features/product-details';
import Footer from './components/Footer';

function App() {
	return (
		<div className='main-container'>
			<SnackbarProvider maxSnack={3} autoHideDuration={1000}>
				<ShoppingProvider>
					<HashRouter>
						<Nav />
						<Routes>
							<Route exact path='/' element={<Home />} />
							<Route exact path='/contacto' element={<Contacto />} />
							<Route
								exact
								path='/ProductDetails/:id'
								element={<ProductDetails />}
							/>
						</Routes>
					</HashRouter>
				</ShoppingProvider>
				<Footer />
			</SnackbarProvider>
		</div>
	);
}

export default App;
