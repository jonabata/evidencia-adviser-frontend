## EVIDENCIA PROYECTO ECOMMERCE

Este proyecto fue iniciado con Create React App y consiste en la primera versión
de un sistema de ecommerce.

En este proyecto evidenciamos el uso de las siguientes tecnologías y
herramientas:

- Fetch API
- Context API
- CSS
- Material UI
- Responsive Design
- Grid
- Flex Box
- ESLint
- Prettier
- React Folder Structure.

### Se está trabajando en la siguientes features:

- Feature para realizar el descuento de los productos directamente en el archivo
  de datos json que se utiliza como mock.
- Guardado de productos ingresados en el cart en el local storage.
- Login.

## Instrucciones

Esta aplicación los datos provienen de datos mockeados, los cuales se encuentran
en el archivo _db.json_ que se encuentra en la carpeta _mocks_

## Script Habilitados

### `npm run mock-api`

Con este script levantamos el mock con los datos para poder hacer el demo de
nuestra aplicación.

### `npm start`

Corre la aplicación en modo desarrollo. Generalmente ingresando
[http://localhost:3000](http://localhost:3000) podemos ver nuestro proyecto en
el navegador.

La aplicación se irá actualizando cada vez que nosotros realicemos cambios y
guardemos.
