# EVIDENCIA CSS PRE-PROCESSORS CON SASS

Este proyecto evidencia lo aprendido respecto a los CSS Pre-Processors y BEM
(CSS Methodologies) con **SASS**.

El objetivo final es evidenciar el trabajo con SASS como CSS Pre-Procesador
implementando la metodología de CSS BEM (Block Element Modifie)

En este ejemplo encontraremos una carpeta llamada **_sass_** la cual contiene
todo nuestros archivos sass, allí encontraremos ejemplos con todos los conceptos
aprendidos tales como:

- Partials.
- Mixins.
- Ciclos.
- neesting.

## Instrucciones

### Pasos para configurar nuestro proyecto:

1. Instalar dependencias del proyecto. Nos dirigimos a la ruta donde descargamos
   el repositorio y tipeamos el comando **_npm install_**

## Script Habilitados

En el proyecto usted puede correr:

### `npm sass`

Este comando ejecutará **SASS** y procesará todos lo que se encuentra en nuestra
carpeta sass (exceptuando los archivos definidos como partials) para entregar un
único archivo css procesado.

También se quedará observando cualquier cambio que realicemos en nuestros
archivos de la carpeta sas para volver a procesar ante cualquier cambio.

> Bonus track: Se encuentra adjunto un archivo de configuración de prettier.
