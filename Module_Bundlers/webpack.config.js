const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (x, config) => {
	let filename =
		config.mode === 'production' ? '[name].bundle.min.js' : '[name].bundle.js';
	console.log(config);
	return {
		entry: { app: './src/app.js' },
		output: {
			path: __dirname + '/build',
			filename,
		},

		devServer: {
			port: 5002,
		},

		module: {
			rules: [
				{
					test: /\.css$/,
					use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
				},
			],
		},

		plugins: [
			new HtmlWebpackPlugin({
				template: './src/index.html',
			}),
		],
	};
};
