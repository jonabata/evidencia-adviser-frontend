import { showAlert } from './messages';
import './styles.css';
import { v4 as uuidv4 } from 'uuid';

document
	.getElementById('btn-alert')
	.addEventListener('click', (id) => showAlert(uuidv4()));
