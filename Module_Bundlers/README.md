# EVIDENCIA MODULE BUNDLERS CON WEBPACK

Este proyecto evidencia lo aprendido respecto a los modules blundlers con
**WebPack**.

El objetivo final es evidenciar el trabajo de WebPack haciendo blundle
(empaquetado) de nuestro código, para ello encontramos dos modulos _app.js_ y
_messages.js_ los cuales utilizan la sentencia import/export. Al ejecutar el
comando webpack se creará una gráfica de todas las dependencias que necesita
nuestra aplicación y generar un único archivo de salida con todos los módulos
necesarios para que nuestra aplicación funcione.

En esta configuración de webpack encontraremos las dependencias dos loaders y un
plugin

## style-loader y css-loader

Al importar estilos en nuestros módulos javascript webpack puede importar los
archivos pero no los puede procesar. Para poder realizar esta tarea existen
estos 2 loaders.

## html-webpack-plugin

Este plugin nos permitirá crear un documento HTML que sirva el bundle y recursos
que hemos generado.

## Instrucciones

### Pasos para configurar nuestro proyecto:

1. Instalar dependencias del proyecto. Nos dirigimos a la ruta donde descargamos
   el repositorio y tipeamos el comando **_npm install_**

## Script Habilitados

En el proyecto usted puede correr:

### `npm js:watch`

Este script ejecuta una script previa _"prejs:watch"_ para eliminar la carpeta
_build_ (la cual será nuestro output), para ello nos pedirá confirmar esta
acción en el CLI. Una vez confirmado ejecutará webpack en modo development,
generará una carpeta con nuestros archivos bundleados.

También se quedará en modo observador para que cuando se realice algun cambio,
se vuelva a ejecutar.

### `js:build`

Este script ejecuta una script previa _"prejs:build"_ para eliminar la carpeta
_build_ (la cual será nuestro output), para ello nos pedirá confirmar esta
acción en el CLI. Una vez confirmado ejecutará webpack en modo production,
generará una carpeta con nuestros archivos bundleados listo para subir nuestro
código productivo.

### `npx webpack serve --mode [development/production]`

Con este comando haremos uso del servidor de desarrollo de la herramienta
webpack-server.

## Configuración del archivo de configuración de WebPack

webpack.config.js

```json
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (x, config) => {
	let filename =
		config.mode === 'production' ? '[name].bundle.min.js' : '[name].bundle.js';
	console.log(config);
	return {
		entry: { app: './src/app.js' },
		output: {
			path: __dirname + '/build',
			filename,
		},

		devServer: {
			port: 5002,
		},

		module: {
			rules: [
				{
					test: /\.css$/,
					use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
				},
			],
		},

		plugins: [
			new HtmlWebpackPlugin({
				template: './src/index.html',
			}),
		],
	};
};
```

> Bonus track: Se encuentra adjunto un archivo de configuración de prettier.
