import { sum } from "./Calculator"

test("probando sum", ()=>{
    const result = sum(2, 1)
    expect(result).toBe(3)
})