const ProductsTable = ({ products, error, deleteProduct }) => {
	return (
		<table border='1' title='list product table'>
			<thead>
				<tr>
					<th>#ID</th>
					<th>PRODUCTO</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{products.length > 0 &&
					products.map((item) => (
						<tr>
							<td>{item.id}</td>
							<td>{item.name}</td>
							<td>
								<button
									aria-label='delete product'
									onClick={() => deleteProduct(item.id)}
									className='btnDel'>
									Eliminar
								</button>
							</td>
						</tr>
					))}
				{error && (
					<tr>
						<td colSpan={2}>Ocurrió un error</td>
					</tr>
				)}
			</tbody>
		</table>
	);
};

export default ProductsTable;
