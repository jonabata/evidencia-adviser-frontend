import { render, screen } from '@testing-library/react';
import Products from './Products';
import ProductsTable from './ProductsTable';

describe('Test view products component rendering', () => {
	it('Sould rendering product form', () => {
		render(<Products />);
		const productForm = screen.getByRole('form', { name: 'form add product' });
		expect(productForm).toBeInTheDocument();
	});

	it('Should rendering table component', () => {
		render(<Products />);

		const table = screen.getByRole('table', { name: 'list product table' });

		expect(table).toBeInTheDocument();
	});

	it('Should rendering delete button', () => {
		render(
			<ProductsTable
				products={[{ id: 1, name: test }]}
				error={false}
				deleteProduct={() => {}}
			/>
		);

		const button = screen.getByRole('button', { name: 'delete product' });

		expect(button).toBeInTheDocument();
	});
});
