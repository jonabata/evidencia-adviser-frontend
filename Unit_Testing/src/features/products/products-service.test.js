import fetchDataService from './products-service.js';

describe('Test get token', () => {
	it('Called get token function', () => {
		const spy = jest.spyOn(fetchDataService, 'getTokens');

		const token = fetchDataService.getTokens();

		expect(spy).toHaveBeenCalled();
		expect(spy).toBeCalledTimes(1);
	});
});

describe('Test different responses from API', () => {
	const productsMock = [
		{ id: 3, name: 'Celular Samsung S22' },
		{ id: 4, name: 'Auriculares JBL' },
	];

	it('Should return products array by response status 200', async () => {
		global.fetch = jest.fn(() =>
			Promise.resolve({
				status: 200,
				json: () => Promise.resolve(productsMock),
			})
		);
		const products = fetchDataService.getProducts(
			'http://localhost:5003/products'
		);

		await expect(products).resolves.toEqual(productsMock);
	});

	it('Should return error 404', async () => {
		global.fetch = jest.fn(() =>
			Promise.resolve({
				status: 404,
				statusText: 'Not Found',
				json: () => Promise.resolve(),
			})
		);
		const response = fetchDataService.getProducts(
			'http://localhost:5003/products'
		);
		await expect(response).rejects.toThrowError('Code: 404 - Not Found');
	});

	it('Should return 500 Internal Server Error', async () => {
		global.fetch = jest.fn(() =>
			Promise.resolve({
				status: 500,
				statusText: 'Internal Server Error',
				json: () => Promise.resolve(),
			})
		);
		const response = fetchDataService.getProducts(
			'http://localhost:5003/products'
		);
		await expect(response).rejects.toThrowError(
			'Code: 500 - Internal Server Error'
		);
	});
});

describe('Test POST and DELETE methods from API', () => {
	it('Method POST to add product', async () => {
		let dataToAdd = { id: 6, name: 'pruebaJona' };

		global.fetch = jest.fn(() =>
			Promise.resolve({
				status: 200,
				method: 'POST',
				json: () => Promise.resolve(dataToAdd),
			})
		);
		const products = fetchDataService.addProducts(
			'http://localhost:5003/products',
			dataToAdd
		);

		await expect(products).resolves.toEqual({ success: true });
	});

	it('Method DELETE to delete product', async () => {
		let id = 2;

		global.fetch = jest.fn(() =>
			Promise.resolve({
				status: 200,
				method: 'DELETE',
				json: () => Promise.resolve({ success: true }),
			})
		);

		let endpoint = `http://localhost:5003/products/${id}`;

		const deleteProducts = fetchDataService.deleteProducts(endpoint);

		await expect(deleteProducts).resolves.toEqual({ success: true });
	});
});
