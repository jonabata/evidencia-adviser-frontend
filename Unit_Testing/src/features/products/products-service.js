const request = async (url, options = {}) => {
	try {
		const res = await fetch(url, options);

		if (res.status >= 200 && res.status < 300) {
			if (options.method === 'GET') return await res.json();
			if (options.method === 'POST') return { success: true };
			if (options.method === 'DELETE') return { success: true };
		}
		if (res.status === 404)
			throw new Error(`Code: ${res.status} - ${res.statusText}`);
		if (res.status === 500)
			throw new Error(`Code: ${res.status} - ${res.statusText}`);
	} catch (error) {
		throw error;
	}
};

const getTokens = () => {
	const TOKEN =
		'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjY0ODI5Mzk5LCJqdGkiOiIzZDU1MWJlMzhlZWY0NmZmYWI2OGU1NTU0YTU4YTYzMCIsInVzZXJfaWQiOjN9.YYXubS7uZBUytsTjBSEoDgJ_LzuFvoDtJpa3Ffn2Pso';

	return TOKEN;
};

const getProducts = async (url) => {
	const options = {
		method: 'GET',
		header: {
			accept: 'application/json',
		},
	};

	return await request(url, options);
};

const addProducts = async (url, { id, name }) => {
	let dataToAdd = { id, name };
	dataToAdd.id = Date.now();

	const options = {
		method: 'POST',
		body: JSON.stringify(dataToAdd),
		headers: {
			'content-type': 'application/json',
		},
	};

	return await request(url, options);
};

const deleteProducts = async (url) => {
	const options = {
		method: 'DELETE',
		headers: {
			'content-type': 'application/json',
		},
	};

	return await request(url, options);
};

export default {
	getProducts,
	getTokens,
	request,
	addProducts,
	deleteProducts,
};
