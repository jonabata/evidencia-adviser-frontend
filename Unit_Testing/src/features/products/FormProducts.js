const FormProducts = ({ addProduct, handleChange, form }) => {
	return (
		<>
			<form aria-label='form add product'>
				<input
					type='text'
					name='name'
					className='input-form-control'
					value={form.name}
					onChange={handleChange}
				/>
				<input
					type='submit'
					value='Agregar'
					onClick={addProduct}
					className='btnSend'
				/>
			</form>
		</>
	);
};

export default FormProducts;
