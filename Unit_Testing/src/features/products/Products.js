import { useState, useEffect } from 'react';
import FormProducts from './FormProducts';
import fetchDataService from './products-service';
import ProductsTable from './ProductsTable';
import './products.css';

const initialValue = [];
const initialForm = { id: null, name: '' };
const Products = () => {
	const [productsDB, setproductsDB] = useState(initialValue);
	const [isError, setisError] = useState(false);
	const [loadDataToRender, setLoadDataToRender] = useState(false);
	const [form, setForm] = useState(initialForm);
	let url = 'http://localhost:5003/products';

	useEffect(() => {
		fetchDataService
			.getProducts(url)
			.then((data) => {
				setproductsDB(data);
				setLoadDataToRender(false);
			})
			.catch((err) => setisError(true));
	}, [loadDataToRender, url]);

	const handleChange = (e) => {
		setForm({ ...form, [e.target.name]: e.target.value });
	};

	const addProduct = (e) => {
		e.preventDefault();
		let dataToAdd = form;
		fetchDataService
			.addProducts(url, dataToAdd)
			.then((data) => {
				setLoadDataToRender(true);
				setForm(initialForm);
			})
			.catch((err) => setisError(true));
	};
	const deleteProduct = (id) => {
		let endpoint = `${url}/${id}`;
		let isDeleted = window.confirm(`Desea eliminar el producto con id: ${id}?`);

		if (isDeleted) {
			fetchDataService
				.deleteProducts(endpoint)
				.then((data) => {
					setLoadDataToRender(true);
				})
				.catch((err) => console.log(err));
		}
	};
	return (
		<>
			<section className='view-products-container'>
				<h2>Productos</h2>
				<FormProducts
					addProduct={(e) => addProduct(e)}
					handleChange={handleChange}
					form={form}
				/>
				<br />
				<ProductsTable
					products={productsDB}
					error={isError}
					deleteProduct={deleteProduct}
				/>
			</section>
		</>
	);
};

export default Products;
