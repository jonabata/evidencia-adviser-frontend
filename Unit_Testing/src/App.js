import logo from './logo.svg';
import './App.css';
import Calculator from './features/calculator';
import Products from './features/products/Products';

function App() {
  return (
    <>
      <Calculator />
      <hr/>
      <Products/>
    </>
  );
}

export default App;
