# EVIDENCIA JEST

Este proyecto fue iniciado con Crate React App y consiste en un ejemplo para evidenciar el trabajo de jest con Unit Testing.

## Instrucciones
El objetivo final es correr los test que se encuentran en este ejemplo, con ello se harán pruebas del servicio y del renderizado de componentes. Estos test utilizan mocks y spy's.

En este repositorio encontraremos 2 test: _products-service.test_ y _products.test_.

###  products-service.test
Realiza un test de los distintos tipos de respuestas que puede darnos nuestra API y también de los métodos POST y DELETE.

###  products.test
Realiza un test del renderizado de la pantalla "Products", para esto a demás de Jest utilizamos **_@testing-library_**.

### Pasos para configurar nuestro proyecto:

1. Instalar dependencias del proyecto. Nos dirigimos a la ruta donde descargamos el repositorio y tipeamos el comando **_npm install_**



## Script Habilitados


En el proyecto usted puede correr: 

### `npm run fake-api`
Este es el primer comando que debemos ejecutar para que nuestra aplicación funcione ya que será el encargado de levantar nuestro mock server, el cual consumirá y modificará el archivo de datos **_products.json_**.


### `npm start`

Corre la aplicación en modo desarrollo.
Generalmente ingresando [http://localhost:3000](http://localhost:3000) podemos ver nuestro proyecto en el navegador.

La aplicación se irá actualizando cada vez que nosotros realicemos cambios y guardemos.


### `npm test`

Lanza los test runner que tenemos en nuestra aplicación, podemos hacerlo de este modo o ejecutando test individual corriendo el comando **_npm run test nombre del test_**


### `npm run build`

Construye la aplicación para producción en la carpeta `build`.\
Empaqueta correctamente React en modo de producción y optimiza la compilación para obtener el mejor rendimiento.


